+++
title = ""
+++

# *Сайт пример*  

*Ниже приведен пример отображения заголовков, ссылок, таблиц, цитат, разделителей, списков и формат кода. Можно изменить как фон сайта, так и текст. 
По сравнению с конструкторами сайтов у нас больше возможностей сделать сайт как хочется, без огранчений. Можно встроить любой модуль, анимацию и прочее.*


## Кто мы?

{{< figure class="avatar" src="/avatar.jpg" alt="avatar">}}

 Мы - это команда профессионалов, которая создает продукты для своих клиентов.
Наши продукты - это не просто игры, это - образ жизни.
Наша цель - сделать игры доступными и интересными для людей всех возрастов.
Что мы предлагаем: - Стабильную заработную плату.
- Официальное трудоустройство, полный соц. пакет.
- График работы: 5/2 с 9:00 до 18:00 (обед с 12:00 до 13:00).
- Обучение за счет компании.
Если вы хотите работать в нашей команде, то ждем ваше резюме с фото.

[Резюме направлять сюда](https://hh.ru/roga-i-kopyta).

## Типография

Это [ссылка](http://google.com). Что-то  *курсивом* или  **жирным**.

Вот таблица:

Year | Award | Category
-----|-------|--------
2014 | Emmy  | Won Outstanding Lead Actor in a miniseries or a movie
2015 | BAFTA | Nominated for Best Leading Actor for Sherlock
2014 | Satellite | Won Best Actor miniseries or television film

Ниже разделитель:

---

Так выглядит цитата:

> To a great mind, nothing is little

Пример `code` блок:

```python
def is_elementary():
  return True
```

## Список

* Foo Bar: Head of Department, Placeholder Names, Lorem
* John Doe: Associate Professor, Department of Computer Science, Ipsum

[^1]: This is the first footnote.
[^2]: This is the second footnote.